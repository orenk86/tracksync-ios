//
//  File.swift
//  TrackSync
//
//  Created by Oren Kosto on 04/05/2018.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation

extension Date {
    static func date(string: String, format: String = "dd-MM-yyyy") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
}

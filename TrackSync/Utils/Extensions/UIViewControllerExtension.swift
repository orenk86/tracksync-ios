//
//  UIViewControllerExtension.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/9/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import UIKit
import Swinject
import SnapKit

extension UIViewController {
    class func container() -> Container {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let dependencyInjector = appDelegate.dependencyInjector else {
                return Container()
        }
        return dependencyInjector.container
    }
    
    class func viewController(for responder: UIResponder) -> UIViewController? {
        guard let next = responder.next else { return nil }
        
        return viewController(for:next)
    }
}

extension UIViewController {
    func fadeIn(duration: TimeInterval = 0.5, completion: (() -> Void)? = nil) {
        view.fadeIn(duration: duration, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, completion: (() -> Void)? = nil) {
        view.fadeOut(duration: duration, completion: completion)
    }
}

extension UIViewController {
    func fullyAddChildViewController(_ childController: UIViewController, toContainerView containerView: UIView? = nil) {
        let superview = containerView ?? self.view
        addChildViewController(childController)
        superview?.addSubview(childController.view)
        childController.didMove(toParentViewController: self)
    }
    
    func fullyRemoveFromParentViewController() {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
}

//
//  StravaNetworkService.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/17/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import StravaSwift

class StravaService: FitnessNetworkServicing {
    var network: Observable<FitnessNetwork> = Observable.just(.Strava)
    var supportedAuthTypes: Observable<[FitnessNetworkAuthType]> = Observable.just([.OAuth])
    var signedIn: Observable<Bool>
    var user: Observable<FitnessNetworkUser?>
    var activities: Observable<[FitnessNetworkActivity]?>
    
    private let config: StravaConfig
    private let strava: StravaClient
    private let userDefaults: UserDefaults
    private let tokenDelegate: TokenDelegate
    private let signedInVariable: Variable<Bool> = Variable(false)
    
    private let disposeBag = DisposeBag()
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
        
        let clientId = Int(FitnessNetwork.Strava.clientId)!
        
        tokenDelegate = StravaTokenDelegate(userDefaults: userDefaults)
        
        config = StravaConfig(clientId: clientId,
                              clientSecret: FitnessNetwork.Strava.secret,
                              redirectUri: "\(FitnessNetwork.Strava.urlScheme)://tracksync.com",
            scope: .viewPrivateWrite,
            delegate: tokenDelegate)
        
        strava = StravaClient.sharedInstance.initWithConfig(config)
        
        signedIn = signedInVariable
            .asObservable()
            .share(replay: 1, scope: .forever)
        
        
        user = signedIn.filter { $0 }
            .flatMap { _ in
                getUser().map { athlete in
                    guard let athlete = athlete else { return nil }
                    return StravaUser(athlete: athlete)
                }
            }.share(replay: 1, scope: .forever)
        
        activities = signedIn.filter { $0 }
            .flatMap { _ in
                getActivities().map { activities in
                    guard let activities = activities else { return nil }
                    return activities.compactMap { activity in
                        StravaActivity(activity: activity)
                    }
                }
            }.share(replay: 1, scope: .forever)
        
        signedInVariable.value = hasStoredToken()
    }
    
    private func hasStoredToken() -> Bool {
        return tokenDelegate.get()?.accessToken != nil
    }
    
    func authenticate(email: String = "", password: String = "") {
        authenticate()
    }
    
    func authenticate() {
        if hasStoredToken() {
            signedInVariable.value = true
        } else {
            strava.authorize()
        }
    }
    
    func handleUrl(_ url: URL) {
        guard let code = strava.handleAuthorizationRedirect(url) else {
            return
        }
        try? strava.getAccessToken(code) { [weak self] token in
            self?.signedInVariable.value = true
        }
    }
}

private class StravaTokenDelegate: TokenDelegate {
    private var token: OAuthToken?
    private let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func get() -> OAuthToken? {
        if self.token?.accessToken == nil,
            let defaultsToken = userDefaults.string(forKey: "\(FitnessNetwork.Strava.rawValue).token") {
            set(OAuthToken(["access_token": defaultsToken,
                            "athlete": [:]]))
        }
        return token
    }
    
    func set(_ token: OAuthToken?) {
        userDefaults.set(token?.accessToken, forKey: "\(FitnessNetwork.Strava.rawValue).token")
        self.token = token
    }
}

private func getUser() -> Observable<Athlete?> {
    return Observable.create({ observer -> Disposable in
        do {
            try StravaClient.sharedInstance.request(Router.athlete, result: { athlete in
                observer.onNext(athlete)
            }, failure: { _ in
                observer.onNext(nil)
            })
        } catch let error {
            debugPrint(error)
            observer.onNext(nil)
        }
        return Disposables.create()
    })
}

private func getActivities() -> Observable<[Activity]?> {
    return Observable.create({ observer -> Disposable in
        do {
            try StravaClient.sharedInstance.request(Router.athleteActivities(params: nil), result: { activities in
                observer.onNext(activities)
            }, failure: { _ in
                observer.onNext(nil)
            })
        } catch let error {
            debugPrint(error)
            observer.onNext(nil)
        }
        return Disposables.create()
    })
}

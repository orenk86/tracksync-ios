//
//  FitnessNetwork.swift
//  TrackSync
//
//  Created by Oren Kosto on 3/29/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation

enum FitnessNetwork: String, EnumCollection {
    case Strava = "Strava"
    case Endomondo = "Endomondo"
    case Unknown = ""
}

extension FitnessNetwork {
    var clientId: String {
        switch self {
        case .Strava:
            return "24526"
        default:
            return ""
        }
    }
    
    var secret: String {
        switch self {
        case .Strava:
            return "a9df63b3f96a8cd7b5c4dda043fb4f896b120f2c"
        default:
            return ""
        }
    }
    
    var accesToken: String {
        switch self {
        case .Strava:
            return "af0961e0b47ec5d8fcddaf906a9ea10c0fd2624a"
        default:
            return ""
        }
    }
    
    var urlScheme: String {
        return "tracksync"
    }
    
    var squareLogoUrl: String {
        switch self {
        case .Strava:
            return "https://is4-ssl.mzstatic.com/image/thumb/Purple128/v4/1c/0a/6c/1c0a6cbd-2bd7-8d61-6f1f-5f5f71d489a4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-4.png/1200x630bb.jpg"
        case .Endomondo:
            return "https://lh3.googleusercontent.com/YpaxxOI4FCcMGLNOmDallw2jy9crQCANOJsbJJt_iMycpdiG0YSsWx7VSvQfautR4u8=s180"
        default:
            return ""
        }
    }
    
    var wideLogoUrl: String {
        switch self {
        case .Strava:
            return "http://cdn.road.cc/sites/default/files/styles/main_width/public/logostrava.png?itok=SRpR-u37"
        case .Endomondo:
            return "http://protechlists.com/wp-content/uploads/2017/05/Endomondo.gif"
        default:
            return ""
        }
    }
}

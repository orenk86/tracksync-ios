//
//  EndomondoLoginViewModel.swift
//  TrackSync
//
//  Created by Oren Kosto on 04/05/2018.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class EndomondoLoginViewModel: LoginViewModeling {
    var network: Driver<FitnessNetwork> = Driver.just(.Endomondo)
    let loginButtonDidTap = PublishSubject<Void>()
    let loggedIn: Driver<Bool>
    
    private let disposeBag = DisposeBag()
    
    init(networkService: FitnessNetworkServicing) {
        loggedIn = networkService.signedIn
            .asDriver(onErrorJustReturn: false)
        
        loginButtonDidTap.subscribe(onNext: { () in
            networkService.authenticate(email: "", password: "")
        }).disposed(by: disposeBag)
    }
}

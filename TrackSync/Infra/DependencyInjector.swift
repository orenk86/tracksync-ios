//
//  DependencyInjector.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/2/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

class DependencyInjector: NSObject {
    
    let container: Container
    
    fileprivate var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    fileprivate var userDefaults: UserDefaults {
        return .standard
    }
    
    override init() {
        container = Container()
        super.init()
        Container.loggingFunction = nil
        registerFitnessNetworks()
        registerViews()
    }
    
}

extension DependencyInjector {
    var stravaService: FitnessNetworkServicing {
        return container.resolve(FitnessNetworkServicing.self,
                                 name: FitnessNetwork.Strava.rawValue)!
    }
    
    var stravaLoginViewController: LoginViewController {
        return container.resolve(LoginViewController.self,
                                 name: FitnessNetwork.Strava.rawValue)!
    }
    
    var stravaActivitiesViewController: ActivitiesViewController {
        return container.resolve(ActivitiesViewController.self,
                                 name: FitnessNetwork.Strava.rawValue)!
    }
    
    var endomondoService: FitnessNetworkServicing {
        return container.resolve(FitnessNetworkServicing.self,
                                 name: FitnessNetwork.Endomondo.rawValue)!
    }
    
    var endomondoNetworkService: NetworkServicing {
        return container.resolve(NetworkServicing.self,
                                 name: FitnessNetwork.Endomondo.rawValue)!
    }
}

private extension DependencyInjector {
    func registerViews() {
        container.register(MainViewModeling.self) { r in
            MainViewModel(networkService: r.resolve(FitnessNetworkServicing.self, name: FitnessNetwork.Endomondo.rawValue)!)
            }.inObjectScope(.weak)
        
        container.storyboardInitCompleted(MainViewController.self) { r, c in
            c.viewModel = r.resolve(MainViewModeling.self)!
        }
        
        //MARK: Login
        container.register(LoginViewModeling.self,
                           name: FitnessNetwork.Strava.rawValue) { _ in
                            StravaLoginViewModel(networkService: self.stravaService)
            }.inObjectScope(.weak)
        
        container.register(LoginViewController.self,
                           name: FitnessNetwork.Strava.rawValue) { r in
                            LoginViewController(viewModel: r.resolve(LoginViewModeling.self,
                                                                     name: FitnessNetwork.Endomondo.rawValue)!)
            }.inObjectScope(.weak)
        
        container.register(LoginViewModeling.self,
                           name: FitnessNetwork.Endomondo.rawValue) { _ in
                            EndomondoLoginViewModel(networkService: self.endomondoService)
            }.inObjectScope(.weak)
        
        container.register(LoginViewController.self,
                           name: FitnessNetwork.Endomondo.rawValue) { r in
                            LoginViewController(viewModel: r.resolve(LoginViewModeling.self,
                                                                     name: FitnessNetwork.Endomondo.rawValue)!)
            }.inObjectScope(.weak)
        
        //MARK: Activities
        container.register(ActivitiesViewModeling.self,
                           name: FitnessNetwork.Strava.rawValue) { _ in
                            StravaActivitiesViewModel(networkService: self.stravaService)
            }.inObjectScope(.weak)
        
        container.register(ActivitiesViewController.self,
                           name: FitnessNetwork.Strava.rawValue) { r in
                            ActivitiesViewController(viewModel: r.resolve(ActivitiesViewModeling.self,
                                                                     name: FitnessNetwork.Strava.rawValue)!)
            }.inObjectScope(.weak)
        
        container.register(ActivitiesViewModeling.self,
                           name: FitnessNetwork.Endomondo.rawValue) { _ in
                            EndomondoActivitiesViewModel(networkService: self.endomondoService)
            }.inObjectScope(.weak)
        
        container.register(ActivitiesViewController.self,
                           name: FitnessNetwork.Endomondo.rawValue) { r in
                            ActivitiesViewController(viewModel: r.resolve(ActivitiesViewModeling.self,
                                                                          name: FitnessNetwork.Endomondo.rawValue)!)
            }.inObjectScope(.weak)
    }
    
    func registerFitnessNetworks() {
        container.register(NetworkServicing.self,
                           name: FitnessNetwork.Endomondo.rawValue) { _ in
                            NetworkService()
            }.inObjectScope(.container)
        
        container.register(FitnessNetworkServicing.self,
                           name: FitnessNetwork.Strava.rawValue) { _ in
                            StravaService(userDefaults: self.userDefaults)
            }.inObjectScope(.container)
        
        container.register(FitnessNetworkServicing.self,
                           name: FitnessNetwork.Endomondo.rawValue) { _ in
                            EndomondoService(userDefaults: self.userDefaults,
                                             networkService: self.endomondoNetworkService)
            }.inObjectScope(.container)
    }
}

//
//  FitnessNetworkUser.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/17/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import StravaSwift

protocol FitnessNetworkUserDataSharable {
    var network: FitnessNetwork { get }
    var userId: String? { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var email: String? { get }
    var imageUrl: URL? { get }
}

protocol FitnessNetworkUser {
    var shared: FitnessNetworkUserSharedData { get }
}

extension FitnessNetworkUser {
    var network: FitnessNetwork {
        return shared.network
    }
    var userId: String? {
        return shared.userId
    }
    var fullName: String? {
        return "\(shared.firstName ?? "") \(shared.lastName ?? "")"
    }
    var firstName: String? {
        return shared.firstName
    }
    var lastName: String? {
        return shared.lastName
    }
    var email: String? {
        return shared.email
    }
    var imageUrl: URL? {
        return shared.imageUrl
    }
}

struct FitnessNetworkUserSharedData: FitnessNetworkUserDataSharable {
    let network: FitnessNetwork
    let userId: String?
    let firstName: String?
    let lastName: String?
    let email: String?
    let imageUrl: URL?
}

struct StravaUser: FitnessNetworkUser {
    let shared: FitnessNetworkUserSharedData
    let originalObject: Athlete
    
    init(athlete: Athlete) {
        originalObject = athlete
        shared = FitnessNetworkUserSharedData(
            network: .Strava,
            userId: "\(athlete.id ?? 0)",
            firstName: athlete.firstname,
            lastName: athlete.lastname,
            email: athlete.email,
            imageUrl: athlete.profile
        )
    }
}

struct EndomondoUser: FitnessNetworkUser {
    let shared: FitnessNetworkUserSharedData
    let originalObject: [String: Any]
    
    init(athlete: [String: Any]) {
        originalObject = athlete
        shared = FitnessNetworkUserSharedData(
            network: .Strava,
            userId: "\(athlete["id"] ?? 0)",
            firstName: "\(athlete["first_name"] ?? "")",
            lastName: "\(athlete["last_name"] ?? "")",
            email: "\(athlete["email"] ?? "")",
            imageUrl: URL(string: "\((athlete["picture"] as? [String: Any])?["url"] ?? "")")
        )
    }
}

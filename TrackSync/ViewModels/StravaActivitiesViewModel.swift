//
//  StravaActivitiesViewModel.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/20/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ActivitiesViewModeling {
    //Output
    var loading: Driver<Bool> { get }
    var network: Driver<FitnessNetwork> { get }
    var activities: Driver<[FitnessNetworkActivity]?> { get }
    var tableData: Driver<[String]> { get }
}

class StravaActivitiesViewModel: ActivitiesViewModeling {
    let network: Driver<FitnessNetwork> = Driver.just(.Strava)
    let loading: Driver<Bool>
    let activities: Driver<[FitnessNetworkActivity]?>
    let tableData: Driver<[String]>
    
    init(networkService: FitnessNetworkServicing) {
        activities = networkService.activities.asDriver(onErrorJustReturn: [])
        
        loading = activities.map { _ in false}
        
        tableData = activities.map { activities in
            guard let activities = activities else { return [] }
            return activities.compactMap { activity in
                activity.name
            }
        }.startWith([])
    }
}

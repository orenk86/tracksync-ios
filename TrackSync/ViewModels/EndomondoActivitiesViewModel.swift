//
//  EndomondoActivitiesViewModel.swift
//  TrackSync
//
//  Created by Oren Kosto on 5/6/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class EndomondoActivitiesViewModel: ActivitiesViewModeling {
    let network: Driver<FitnessNetwork> = Driver.just(.Endomondo)
    let loading: Driver<Bool> = Driver.just(false)
    let activities: Driver<[FitnessNetworkActivity]?>
    let tableData: Driver<[String]>
    
    init(networkService: FitnessNetworkServicing) {
        activities = networkService.activities.asDriver(onErrorJustReturn: [])
        
        tableData = activities.map { activities in
            guard let activities = activities else { return [] }
            return activities.compactMap { activity in
                activity.name
            }
        }.startWith([])
    }
}

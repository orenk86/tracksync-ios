//
// Created by Oren Kosto on 4/12/18.
// Copyright (c) 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol LoginViewModeling {
    //input
    var loginButtonDidTap: PublishSubject<Void> { get }

    //Output
    var network: Driver<FitnessNetwork> { get }
    var loggedIn: Driver<Bool> { get }
}

class StravaLoginViewModel: LoginViewModeling {
    var network: Driver<FitnessNetwork> = Driver.just(.Strava)
    let loginButtonDidTap = PublishSubject<Void>()
    let loggedIn: Driver<Bool>
    
    private let disposeBag = DisposeBag()

    init(networkService: FitnessNetworkServicing) {
        loggedIn = networkService.signedIn
            .asDriver(onErrorJustReturn: false)

        loginButtonDidTap.subscribe(onNext: { () in
            networkService.authenticate()
        }).disposed(by: disposeBag)
    }
}

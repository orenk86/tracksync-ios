//
//  EndomondoService.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/25/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

enum EndomondoApi {
    case auth(email: String, password: String)
    case user(userId: String)
    case activities
    case activity(id: String, method: NetworkMethod)
    
    var baseUrl: String {
        switch self {
        case .user:
            return "https://www.endomondo.com/"
        default:
            return "https://api.mobile.endomondo.com/"
        }
        
    }
    
    var endpoint: String {
        switch self {
        case .auth:
            return "mobile/auth"
        case .user(let userId):
            return "/rest/v1/users/\(userId)"
        case .activities:
            return "mobile/api/workout/list"
        case .activity(_, let method):
            switch method {
            case .get:
                return "mobile/api/workout/get"
            case .post:
                return "mobile/track"
            default:
                return ""
            }
        }
    }
    
    var url: String {
        return "\(self.baseUrl)\(self.endpoint)"
    }
    
    var params: [String: String] {
        switch self {
        case .auth(let email, let password):
            return [
                "email": email,
                "password": password,
                "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
                "country": Locale.current.regionCode ?? "",
                "action": "pair"
            ]
        default:
            return [:]
        }
    }
    
}

private class EndomondoTokenDelegate {
    private var token: [String: String]?
    private let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func get() -> [String: String]? {
        if self.token == nil,
            let defaultsToken = userDefaults
                .dictionary(forKey: "\(FitnessNetwork.Endomondo.rawValue).token") as? [String: String],
            defaultsToken.count > 0 {
            set(defaultsToken)
        }
        return token
    }
    
    func set(_ token: [String: String]) {
        userDefaults.set(token, forKey: "\(FitnessNetwork.Endomondo.rawValue).token")
        self.token = token
    }
}

class EndomondoService: FitnessNetworkServicing {
    
    var network: Observable<FitnessNetwork> = Observable.just(.Endomondo)
    var supportedAuthTypes: Observable<[FitnessNetworkAuthType]> = Observable.just([.EmailPassword])
    var signedIn: Observable<Bool>
    var user: Observable<FitnessNetworkUser?>
    var activities: Observable<[FitnessNetworkActivity]?> = Observable.just(nil)
    
    private let userDefaults: UserDefaults
    private let signIn = PublishSubject<(email: String, password: String)>()
    private let tokenDelegate: EndomondoTokenDelegate
    private let signedInVariable: Variable<Bool> = Variable(false)
    
    private let disposeBag = DisposeBag()
    
    init(userDefaults: UserDefaults,
         networkService: NetworkServicing) {
        self.userDefaults = userDefaults
        self.tokenDelegate = EndomondoTokenDelegate(userDefaults: userDefaults)
        
        let signInApiCall = signIn.asObservable()
            .flatMap({ (email, password) in
                return endomondoSignIn(with: email, password: password, network: networkService)
            })
            .map({ [tokenDelegate] response in
                guard let response = response else { return false }
                tokenDelegate.set(response)
                return response.count > 0
            })
            .startWith(false)
        
        signedIn = Observable.merge(signInApiCall, signedInVariable.asObservable())
            .share(replay: 1, scope: .forever)
        
        user = signedIn.filter { $0 }
            .flatMap { [tokenDelegate, networkService] _ in
                getUser(tokenDelegate: tokenDelegate, network: networkService).map { user in
                    guard let user = user else { return nil }
                    return EndomondoUser(athlete: user)
                }
            }.share(replay: 1, scope: .forever)
        
        activities = signedIn.filter { $0 }
            .flatMap { [tokenDelegate, networkService] _ in
                getActivities(tokenDelegate: tokenDelegate, network: networkService).map { activities in
                    guard let activities = activities else { return nil }
                    return activities.compactMap { activity in
                        EndomondoActivity(activity: activity)
                    }
                }
            }.share(replay: 1, scope: .forever)
        
        signedInVariable.value = hasStoredToken()
    }
    
    private func hasStoredToken() -> Bool {
        return tokenDelegate.get() != nil
    }
    
    func authenticate() {
        authenticate(email: "", password: "")
    }
    
    func authenticate(email: String, password: String) {
        if hasStoredToken() {
            signedInVariable.value = true
        } else {
            signIn.onNext((email, password))
        }
    }
    
    func handleUrl(_ url: URL) {
        
    }
}

private func endomondoSignIn(with email: String, password: String, network: NetworkServicing) -> Observable<[String: String]?> {
    let api = EndomondoApi.auth(email: email, password: password)
    return network.requestString(method: .get, url: api.url , params: api.params)
        .map({ response in
            return loginParams(from: response)
        })
}

private func loginParams(from response: String) -> [String: String] {
    let params = response.split(separator: "\n")
    var result: [String: String] = [:]
    for param in params {
        let paramElements = param.split(separator: "=")
        if paramElements.count == 2 {
            result["\(paramElements[0])"] = "\(paramElements[1])"
        }
    }
    
    return result
}

private func getUser(userId: String? = nil, tokenDelegate: EndomondoTokenDelegate, network: NetworkServicing) -> Observable<[String: Any]?> {
    var finalUserId = userId
    let token = tokenDelegate.get()
    if finalUserId == nil {
        if let tokenUserId = token?["userId"] {
            finalUserId = tokenUserId
        } else {
            return Observable.never()
        }
    }
    
    let authToken = token?["authToken"]
    let api = EndomondoApi.user(userId: finalUserId!)
    return network.request(method: .get, url: api.url, params: ["authToken": authToken ?? ""])
        .map({ response in
            return response as? [String: Any]
    })
}

private func getActivities(tokenDelegate: EndomondoTokenDelegate, network: NetworkServicing) -> Observable<[JSON]?> {
    let token = tokenDelegate.get()
    
    let authToken = token?["authToken"]
    let api = EndomondoApi.activities
    return network.request(method: .get, url: api.url, params: ["authToken": authToken ?? ""])
        .map({ response in
            let json = JSON(response)
            return json["data"].array
        })
}

//
//  FitnessNetworkActivity.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/21/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import StravaSwift
import SwiftyJSON

protocol FitnessNetworkActivityDataSharable {
    var network: FitnessNetwork { get }
    var activityId: String? { get }
    var name: String? { get }
    var startDate: Date? { get }
    var distance: Double? { get }
    var movingTime: TimeInterval? { get }
}

protocol FitnessNetworkActivity {
    var shared: FitnessNetworkActivitySharedData { get }
}

extension FitnessNetworkActivity {
    var network: FitnessNetwork {
        return shared.network
    }
    var activityId: String? {
        return shared.activityId
    }
    var name: String? {
        return shared.name
    }
    var startDate: Date? {
        return shared.startDate
    }
    var distance: Double? {
        return shared.distance
    }
    var movingTime: TimeInterval? {
        return shared.movingTime
    }
}

struct FitnessNetworkActivitySharedData: FitnessNetworkActivityDataSharable {
    let network: FitnessNetwork
    let activityId: String?
    let name: String?
    let startDate: Date?
    let distance: Double?
    let movingTime: TimeInterval?
}

struct StravaActivity: FitnessNetworkActivity {
    let shared: FitnessNetworkActivitySharedData
    let originalObject: Activity
    
    init(activity: Activity) {
        originalObject = activity
        shared = FitnessNetworkActivitySharedData(
            network: .Strava,
            activityId: "\(activity.id ?? 0)",
            name: activity.name,
            startDate: activity.startDate,
            distance: activity.distance,
            movingTime: activity.movingTime
        )
    }
}

struct EndomondoActivity: FitnessNetworkActivity {
    let shared: FitnessNetworkActivitySharedData
    let originalObject: JSON
    
    init(activity: JSON) {
        originalObject = activity
        shared = FitnessNetworkActivitySharedData(
            network: .Endomondo,
            activityId: "\(activity["id"].numberValue)",
            name: "\(activity["id"].numberValue)",
            startDate: Date.date(string: activity["start_time"].string ?? "",
                            format: "yyyy-MM-dd HH:mm:ss z"),
            distance: activity["distance_km"].double,
            movingTime: activity["duration_sec"].double
        )
    }
}

//
//  UIViewExtension.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/9/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import UIKit
import Swinject

extension UIView {
    private class func container() -> Container {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let dependencyInjector = appDelegate.dependencyInjector else {
                return Container()
        }
        return dependencyInjector.container
    }
    
    func fadeOut(duration: TimeInterval = 0.5, completion: (() -> Void)? = nil) {
        if isHidden {
            return
        }
        
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 0.0
        }) { [weak self] _ in
            self?.isHidden = true
            completion?()
        }
    }
    
    func fadeIn(duration: TimeInterval = 0.5, completion: (() -> Void)? = nil) {
        if !isHidden && alpha == 1.0 {
            return
        }
        if isHidden && alpha != 0.0 {
            alpha = 0.0
        }
        isHidden = false
        
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 1.0
            
        }) { _ in
            completion?()
        }
    }
    
    func viewController() -> UIViewController? {
        return UIViewController.viewController(for: self)
    }
}

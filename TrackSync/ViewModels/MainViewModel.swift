//
//  MainViewModel.swift
//  TrackSync
//
//  Created by Oren Kosto on 10/04/2018.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol MainViewModeling {
    var loading: Driver<Bool> { get }
    var presentLoginView: Driver<Void> { get }
}

class MainViewModel: MainViewModeling {
    let loading: Driver<Bool> = Driver.just(true)
    let presentLoginView: Driver<Void>

    init(networkService: FitnessNetworkServicing) {
        presentLoginView = networkService.signedIn
            .map { _ in return }
            .asDriver(onErrorJustReturn: ())
    }
}

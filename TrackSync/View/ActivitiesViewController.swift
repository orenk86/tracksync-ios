//
//  ActivitiesViewController.swift
//  TrackSync
//
//  Created by Oren Kosto on 4/20/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources

class ActivitiesViewController: UIViewController, LoadingIndicatorPresentable {
    
    var viewModel: ActivitiesViewModeling!
    
    let disposeBag = DisposeBag()
    
    internal let loadingIndicator = UIActivityIndicatorView()
    let tableView = UITableView()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: ActivitiesViewModeling) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupBindings()
    }
    
    private func setupView() {
        view.addSubview(tableView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = 50.0
        tableView.snp.makeConstraints { make in
            make.top.equalTo(view).offset(20)
            make.left.equalTo(view)
            make.bottom.equalTo(view)
            make.right.equalTo(view)
        }
        
        setupLoadingIndicator(container: view)
    }
    
    private func setupBindings() {
        viewModel.loading
            .drive(onNext: { [weak self] in
                self?.setLoading(loading: $0)
            })
            .disposed(by: disposeBag)
        
        viewModel.activities
            .drive(onNext: { activities in
                guard let activities = activities else { return }
                print("Got activities:")
                for activity in activities {
                    if let name = activity.name {
                        print(name)
                    }
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.tableData.asObservable().bind(to: tableView.rx.items(cellIdentifier: "Cell")) { index, model, cell in
            cell.textLabel?.text = model
            }
            .disposed(by: disposeBag)
    }
}

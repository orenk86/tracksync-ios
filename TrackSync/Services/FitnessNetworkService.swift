//
//  FitnessNetworkService.swift
//  TrackSync
//
//  Created by Oren Kosto on 3/29/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import Foundation
import RxSwift

enum FitnessNetworkAuthType {
    case OAuth
    case EmailPassword
}

protocol FitnessNetworkServicing {
    var network: Observable<FitnessNetwork> { get }
    var supportedAuthTypes: Observable<[FitnessNetworkAuthType]> { get }
    var signedIn: Observable<Bool> { get }
    var user: Observable<FitnessNetworkUser?> { get }
    var activities: Observable<[FitnessNetworkActivity]?> { get }
    
    func authenticate()
    func authenticate(email: String, password: String)
    func handleUrl(_ url: URL)
}

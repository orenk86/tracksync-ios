//
// Created by Oren Kosto on 12/04/2018.
// Copyright (c) 2018 Oren Kosto. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController, LoadingIndicatorPresentable, LoginButtonPresentable, KeyboardPresentable {
    var viewModel: LoginViewModeling!
    let disposeBag = DisposeBag()
    
    let loadingIndicator = UIActivityIndicatorView()
    let scrollView = UIScrollView()
    let contentView = UIView()
    let emailField: UITextField = UITextField()
    let passwordField: UITextField = UITextField()
    let loginButton = UIButton()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: LoginViewModeling) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupBindings()
    }
    
    private func setupView() {
        setupLoadingIndicator(container: view)
        setupLoginButton()
        setupKeyboard(scrollView: scrollView)
    }
    
    private func setupBindings() {
        viewModel.loggedIn.drive(onNext: { [weak self] loggedIn in
//            self?.emailField.isHidden = loggedIn
//            self?.passwordField.isHidden = loggedIn
//            self?.loginButton.isHidden = loggedIn
            self?.setLoading(loading: !loggedIn)
        }).disposed(by: disposeBag)
        
        loginButton.rx
            .controlEvent(.touchUpInside)
            .bind { [weak self] in
                self?.viewModel.loginButtonDidTap.onNext(())
            }.disposed(by: disposeBag)
    }
}

protocol LoginButtonPresentable {
    var scrollView: UIScrollView { get }
    var contentView: UIView { get }
    var emailField: UITextField { get }
    var passwordField: UITextField { get }
    var loginButton: UIButton { get }
}

extension LoginButtonPresentable where Self: UIViewController {
    func setupLoginButton() {
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { maker in
            maker.edges.equalTo(view)
        }
        
        scrollView.addSubview(contentView)
        
        contentView.snp.makeConstraints { maker in
            maker.top.bottom.equalTo(scrollView)
            maker.left.right.equalTo(view)
        }
        
        contentView.addSubview(emailField)
        contentView.addSubview(passwordField)
        contentView.addSubview(loginButton)
        
        emailField.backgroundColor = UIColor.gray
        passwordField.backgroundColor = UIColor.gray
        loginButton.backgroundColor = UIColor.cyan
        loginButton.tintColor = UIColor.darkText
        loginButton.setTitle("Login", for: .normal)
        
        loginButton.snp.makeConstraints { maker in
            maker.height.equalTo(50)
            maker.left.right.equalTo(contentView).inset(100)
            maker.bottom.equalTo(view).inset(50)
        }
        
        passwordField.snp.makeConstraints { maker in
            maker.height.equalTo(loginButton)
            maker.width.equalTo(loginButton).offset(50)
            maker.centerX.equalTo(loginButton)
            maker.bottom.equalTo(loginButton.snp.top).offset(-20)
        }
        
        emailField.snp.makeConstraints { maker in
            maker.height.equalTo(passwordField)
            maker.width.equalTo(passwordField)
            maker.centerX.equalTo(passwordField)
            maker.bottom.equalTo(passwordField.snp.top).offset(-20)
        }
    }
}

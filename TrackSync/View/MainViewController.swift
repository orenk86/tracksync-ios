//
//  ViewController.swift
//  TrackSync
//
//  Created by Oren Kosto on 3/28/18.
//  Copyright © 2018 Oren Kosto. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxKeyboard

class MainViewController: UIViewController, LoadingIndicatorPresentable {

    var viewModel: MainViewModeling!

    let disposeBag = DisposeBag()

    internal let loadingIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupView()
        setupBindings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupView() {
        
        setupLoadingIndicator(container: view)
    }

    private func setupBindings() {
        viewModel.loading
                .drive(onNext: { [weak self] in
                    self?.setLoading(loading: $0)
                })
                .disposed(by: disposeBag)
        
        viewModel.presentLoginView.drive(onNext: { [weak self] in
            self?.presentLoginView()
        }).disposed(by: disposeBag)
    }

    func presentLoginView() {
        let controller = UIViewController.container().resolve(LoginViewController.self,
                                                              name: FitnessNetwork.Strava.rawValue)!
        
        fullyAddChildViewController(controller)
    }
}

protocol LoadingIndicatorPresentable {
    var loadingIndicator: UIActivityIndicatorView { get }
}

extension LoadingIndicatorPresentable where Self: UIViewController {
    
    func setupLoadingIndicator(container: UIView) {
        container.addSubview(loadingIndicator)
        container.bringSubview(toFront: loadingIndicator)
        loadingIndicator.activityIndicatorViewStyle = .whiteLarge
        loadingIndicator.color = UIColor.black
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.snp.makeConstraints { make in
            make.center.equalTo(container)
        }
    }
    
    func setLoading(loading: Bool) {
        if loading {
            loadingIndicator.startAnimating()
        } else {
            loadingIndicator.stopAnimating()
        }
    }
}

protocol KeyboardPresentable {
    var disposeBag: DisposeBag { get }
}

extension KeyboardPresentable where Self: UIViewController {
    func setupKeyboard(scrollView: UIScrollView) {
//        RxKeyboard.instance.visibleHeight
//            .drive(onNext: { keyboardVisibleHeight in
//                scrollView.contentInset.bottom = keyboardVisibleHeight
//            })
//            .disposed(by: disposeBag)
    }
}
